package Model;

public class Tenista extends Atleta{

	//Atributos
	private int forcaSaque; // 1- muito fraco ,2- fraco,3- medio , 4 - forte, 5- muito forte
	private char categoria; // A= Amador P= Profissional
	private boolean estilo; // C= Canhoto D= Destro 
	private int totalVitorias; 

	//Construtor
	
	public Tenista(String nome, int forca) { 
		super(nome);
		this.forcaSaque = forca;
	}
	
	//Setters e Getters
public int getForcaSaque() {
	return forcaSaque;
}
public void setForcaSaque(int forcaSaque) {
	this.forcaSaque = forcaSaque;
}
public char getCategoria() {
	return categoria;
}
public void setCategoria(char categoria) {
	this.categoria = categoria;
}
public boolean getEstilo() {
	return estilo;
}
public void setEstilo(boolean estilo) {
	this.estilo = estilo;
}
public int getTotalVitorias() {
	return totalVitorias;
}
public void setTotalVitorias(int totalVitorias) {
	this.totalVitorias = totalVitorias;
}
}