package View;

import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.StyledDocument;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

import java.awt.Font;

import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import Model.Endereco;
import Model.Futebolista;
import Model.Tenista;

import java.awt.event.ActionListener;

import Model.*;
import Controller.*;

import javax.swing.JTextPane;



public class Cadastro_Atletas extends JFrame {

	private JPanel contentPane;
	private JTextField textNome;
	private JTextField textIdade;
	private JTextField textPeso;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField textEspecifico;
	private JTextField textCep;
	private JTextField textEstado;
	private JTextField textCidade;
	private final Action action = new SwingAction();
	
	private	boolean fut = false;
	private boolean tenis = false;
	Futebolista umFutebolista = null;
	Tenista umTenista = null;
	Endereco umEndereco = null;
	private JTextField textDados;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cadastro_Atletas frame = new Cadastro_Atletas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	 * Create the frame.
	 */
	public Cadastro_Atletas() {
		
		final ControladorAtleta umControlador = new ControladorAtleta();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 510, 429);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDesejaCadastrarQual = new JLabel("Deseja cadastrar qual atleta?");
		lblDesejaCadastrarQual.setBounds(12, 12, 220, 15);
		contentPane.add(lblDesejaCadastrarQual);
		
		
		final JLabel lblNome = new JLabel("Nome");
		lblNome.setEnabled(false);
		lblNome.setBounds(372, 31, 70, 15);
		contentPane.add(lblNome);
		
		textNome = new JTextField();
		textNome.setEnabled(false);
		textNome.setBounds(289, 43, 199, 19);
		contentPane.add(textNome);
		textNome.setColumns(10);
		
		final JLabel lblIdade = new JLabel("Idade");
		lblIdade.setEnabled(false);
		lblIdade.setBounds(372, 60, 70, 15);
		contentPane.add(lblIdade);
		
		textIdade = new JTextField();
		textIdade.setEnabled(false);
		textIdade.setBounds(338, 74, 114, 19);
		contentPane.add(textIdade);
		textIdade.setColumns(10);
		
		final JLabel lblPeso = new JLabel("Peso");
		lblPeso.setEnabled(false);
		lblPeso.setBounds(372, 94, 70, 15);
		contentPane.add(lblPeso);
		
		textPeso = new JTextField();
		textPeso.setEnabled(false);
		textPeso.setBounds(338, 109, 114, 19);
		contentPane.add(textPeso);
		textPeso.setColumns(10);
		
		final JRadioButtonMenuItem rdbtnmntmCanhoto = new JRadioButtonMenuItem("Canhoto");
		rdbtnmntmCanhoto.setEnabled(false);
		buttonGroup_1.add(rdbtnmntmCanhoto);
		rdbtnmntmCanhoto.setBounds(305, 138, 95, 19);
		contentPane.add(rdbtnmntmCanhoto);
		
		final JRadioButtonMenuItem rdbtnmntmDestro = new JRadioButtonMenuItem("Destro");
		rdbtnmntmDestro.setEnabled(false);
		buttonGroup_1.add(rdbtnmntmDestro);
		rdbtnmntmDestro.setBounds(400, 138, 95, 19);
		contentPane.add(rdbtnmntmDestro);
		
		final JLabel lblEspecifico = new JLabel("      Especifico");
		lblEspecifico.setEnabled(false);
		lblEspecifico.setBounds(338, 159, 130, 15);
		contentPane.add(lblEspecifico);
		
		textEspecifico = new JTextField();
		textEspecifico.setEnabled(false);
		textEspecifico.setBounds(338, 175, 114, 19);
		contentPane.add(textEspecifico);
		textEspecifico.setColumns(10);
		
		final JLabel lblCep = new JLabel("CEP");
		lblCep.setEnabled(false);
		lblCep.setBounds(382, 228, 77, 15);
		contentPane.add(lblCep);
		
		textCep = new JTextField();
		textCep.setEnabled(false);
		textCep.setBounds(338, 241, 114, 19);
		contentPane.add(textCep);
		textCep.setColumns(10);
		
		final JLabel lblEstado = new JLabel("Estado");
		lblEstado.setEnabled(false);
		lblEstado.setBounds(368, 259, 70, 15);
		contentPane.add(lblEstado);
		
		textEstado = new JTextField();
		textEstado.setEnabled(false);
		textEstado.setBounds(338, 272, 114, 19);
		contentPane.add(textEstado);
		textEstado.setColumns(10);
		
		final JLabel lblCidade = new JLabel("Cidade");
		lblCidade.setEnabled(false);
		lblCidade.setBounds(372, 289, 70, 15);
		contentPane.add(lblCidade);
		
		textCidade = new JTextField();
		textCidade.setEnabled(false);
		textCidade.setColumns(10);
		textCidade.setBounds(338, 303, 114, 19);
		contentPane.add(textCidade);
		
		final JLabel lblEndereco = new JLabel("ENDERECO");
		lblEndereco.setEnabled(false);
		lblEndereco.setFont(new Font("Dialog", Font.BOLD, 13));
		lblEndereco.setBounds(356, 201, 96, 15);
		contentPane.add(lblEndereco);
		
		final JTextField textDados = new JTextField();
		textDados.setEnabled(false);
		textDados.setBounds(50, 210, 182, 19);
		contentPane.add(textDados);
		textDados.setColumns(10);
		
		final JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setEnabled(false);
		textPane.setBounds(12, 272, 265, 141);
		contentPane.add(textPane);
		// BOTOES
		
		final JButton btnFutebolista = new JButton("Futebolista");
		final JButton btnTenista = new JButton("Tenista");
		final JButton btnSubmeter = new JButton("Submeter");
		
		final JButton btnListarAtletas = new JButton("Listar Atletas");
		btnListarAtletas.setEnabled(false);
		btnListarAtletas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				textPane.setText("");
			StyledDocument doc = textPane.getStyledDocument();
			try {
				doc.insertString(doc.getLength(),umControlador.exibirT() + "\n" + umControlador.exibirF(), null);

			} 
			catch(Exception e) { JOptionPane.showMessageDialog(null, "Erro");}
			}

			
		});
		btnListarAtletas.setBounds(140, 141, 130, 25);
		contentPane.add(btnListarAtletas);
		
		final JButton btnRemoverAtleta = new JButton("Remover");
		btnRemoverAtleta.setEnabled(false);
		btnRemoverAtleta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(umControlador.remover(textDados.getText()))
				JOptionPane.showMessageDialog(null, "Removido com Sucesso!");
				else
					JOptionPane.showMessageDialog(null, "Não Encontrado");
			}
		});
		btnRemoverAtleta.setBounds(12, 238, 117, 25);
		contentPane.add(btnRemoverAtleta);
		
		
		final JButton btnBuscarAtleta = new JButton("Buscar Atleta");
		btnBuscarAtleta.setEnabled(false);
		btnBuscarAtleta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				textPane.setText("");
			StyledDocument doc = textPane.getStyledDocument();
			try {
				doc.insertString(doc.getLength(),umControlador.caract(textDados.getText()), null);

			} 
			catch(Exception e) { JOptionPane.showMessageDialog(null, "Erro");}
			}
		});
		btnBuscarAtleta.setFont(new Font("Dialog", Font.BOLD, 11));
		btnBuscarAtleta.setBounds(143, 239, 120, 25);
		contentPane.add(btnBuscarAtleta);
		
		final JLabel lblRemoveBusca = new JLabel("Aleta a remover ou Buscar:");
		lblRemoveBusca.setEnabled(false);
		lblRemoveBusca.setBounds(33, 178, 254, 34);
		contentPane.add(lblRemoveBusca);
		
	
		
		
		btnSubmeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (fut){
					umFutebolista = new Futebolista(textNome.getText(), textEspecifico.getText());
					umFutebolista.setIdade(textIdade.getText());
					umFutebolista.setPeso(textPeso.getText());
					umFutebolista.setDestro(rdbtnmntmDestro.isEnabled());
			//		umFutebolista.setPosicao(textField_3.getText());
					umEndereco = new Endereco(textCep.getText());
					umEndereco.setEstado(textEstado.getText());
					umEndereco.setCidade(textCidade.getText());
					
					umFutebolista.setEndereco(umEndereco);
					umControlador.adicionarF(umFutebolista);	
					
					textNome.setText("");
					textIdade.setText("");
					textPeso.setText("");
					textEspecifico.setText("");
					textCep.setText("");
					textEstado.setText("");
					textCidade.setText("");
					
					
					JOptionPane.showMessageDialog(null, "Futebolista adicionado com Sucesso!");
				}
				if (tenis){
					umTenista = new Tenista(textNome.getText(), Integer.parseInt(textEspecifico.getText()));
					umTenista.setIdade(textIdade.getText());
					umTenista.setPeso(textPeso.getText());
					umTenista.setEstilo(rdbtnmntmDestro.isEnabled());
					umEndereco = new Endereco(textCep.getText());
					umEndereco.setEstado(textEstado.getText());
					umEndereco.setCidade(textCidade.getText());
					
					umTenista.setEndereco(umEndereco);
					umControlador.adicionarT(umTenista);	
					
					textNome.setText("");
					textIdade.setText("");
					textPeso.setText("");
					textEspecifico.setText("");
					textCep.setText("");
					textEstado.setText("");
					textCidade.setText("");
					
					
					JOptionPane.showMessageDialog(null, "Tenista adicionado com Sucesso!");
				}
			}
		});
		btnSubmeter.setEnabled(false);
		btnSubmeter.setBounds(12, 141, 117, 25);
		contentPane.add(btnSubmeter);
		
		
		
		
		btnFutebolista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNome.setEnabled(true);
				textNome.setEnabled(true);
				lblIdade.setEnabled(true);
				textIdade.setEnabled(true);
				lblPeso.setEnabled(true);
				textPeso.setEnabled(true);
				rdbtnmntmCanhoto.setEnabled(true);
				rdbtnmntmDestro.setEnabled(true);
				
				lblEspecifico.setEnabled(true);
				textEspecifico.setEnabled(true);
				lblEndereco.setEnabled(true);
				lblCep.setEnabled(true);
				textCep.setEnabled(true);
				lblEstado.setEnabled(true);
				textEstado.setEnabled(true);
				lblCidade.setEnabled(true);
				textCidade.setEnabled(true);
				lblEspecifico.setText("Posicao em campo");
				btnFutebolista.setEnabled(false);
				btnTenista.setEnabled(true);
				btnSubmeter.setEnabled(true);
				
				btnListarAtletas.setEnabled(true);
				btnRemoverAtleta.setEnabled(true);
				textDados.setEnabled(true);
				textPane.setEnabled(true);
				btnBuscarAtleta.setEnabled(true);
				lblRemoveBusca.setEnabled(true);
				
				fut=true;
				tenis=false;
				
		//		textPane.
				
			}
		});
		btnFutebolista.setBounds(65, 39, 117, 25);
		contentPane.add(btnFutebolista);
		

		btnTenista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNome.setEnabled(true);
				textNome.setEnabled(true);
				lblIdade.setEnabled(true);
				textIdade.setEnabled(true);
				lblPeso.setEnabled(true);
				textPeso.setEnabled(true);
				rdbtnmntmCanhoto.setEnabled(true);
				rdbtnmntmDestro.setEnabled(true);
				
				lblEspecifico.setEnabled(true);
				textEspecifico.setEnabled(true);
				lblEndereco.setEnabled(true);
				lblCep.setEnabled(true);
				textCep.setEnabled(true);
				lblEstado.setEnabled(true);
				textEstado.setEnabled(true);
				lblCidade.setEnabled(true);
				textCidade.setEnabled(true);
				lblEspecifico.setText("Forca saque 1-5");
				btnFutebolista.setEnabled(true);
				btnTenista.setEnabled(false);
				btnSubmeter.setEnabled(true);
				
				btnListarAtletas.setEnabled(true);
				btnRemoverAtleta.setEnabled(true);
				textDados.setEnabled(true);
				textPane.setEnabled(true);
				btnBuscarAtleta.setEnabled(true);
				lblRemoveBusca.setEnabled(true);
				
				tenis=true;
				fut =false;
			}
		});
		btnTenista.setBounds(65, 75, 117, 25);
		contentPane.add(btnTenista);
		
		JLabel lblCadastro = new JLabel("CADASTRO");
		lblCadastro.setBounds(356, 7, 82, 15);
		contentPane.add(lblCadastro);
		
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
