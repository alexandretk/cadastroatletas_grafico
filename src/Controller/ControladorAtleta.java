package Controller;

//Imports
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Model.*;


public class ControladorAtleta {

	//Definindo Array
	static ArrayList <Futebolista> listaFut;
	static ArrayList <Tenista> listaTenis;
	
	
	public ControladorAtleta(){
		listaFut = new ArrayList<Futebolista>();
		listaTenis = new ArrayList<Tenista>();
	}
	

	public void adicionarF(Futebolista umFutebolista){
//		JOptionPane.showMessageDialog(null, "Adicionando Futebolista...");
		listaFut.add(umFutebolista);
//		JOptionPane.showMessageDialog(null, "Adicionado com Sucesso!");
	}
	
	public void adicionarT(Tenista umTenista){
//		JOptionPane.showMessageDialog(null, "Adicionando Tenista...");
		listaTenis.add(umTenista);
//		JOptionPane.showMessageDialog(null, "Adicionado com Sucesso!");
	}	
	
	public void removerT(Tenista umTenista){
		if (umTenista == null)
		{ 
			JOptionPane.showMessageDialog(null, "Nao encontrado.");
			
		}else
		{
//		System.out.println("Removendo Tenista...");
		listaTenis.remove(umTenista);
//		JOptionPane.showMessageDialog(null, "Tenista removido com Sucesso!");
		}
	}
	
	public void removerF(Futebolista umFutebolista){
		if (umFutebolista == null)
		{ 
	//		JOptionPane.showMessageDialog(null, "Nao encontrado.");
			
		}else
		{
	//	JOptionPane.showMessageDialog(null, "Removendo Futebolista...");
		listaFut.remove(umFutebolista);
	//	JOptionPane.showMessageDialog(null, "Atleta removido com Sucesso!"); 
		} 
	}
	
	
		public boolean remover(String nome){
			Tenista umTenista= null;
			Futebolista umFutebolista=null;
		
	umFutebolista = pesquisarF(nome);
	umTenista = pesquisarT(nome);
	
	if ((umFutebolista == null) && (umTenista == null)) {
//			JOptionPane.showMessageDialog(null, "Nao encontrado");
		return false;
			} else{
	if ((umFutebolista != null) && (umTenista != null)) 
		{
		
		removerF(umFutebolista);
		removerT(umTenista);
	//	JOptionPane.showMessageDialog(null, "Atleta Futebolista e Tenista Deletado");
		return true;
		} else{
	if (umTenista != null)
	{
		removerT(umTenista);
	//	JOptionPane.showMessageDialog(null, "Tenista Deletado");
		return true;
	} else {
		removerF(umFutebolista);
	//	JOptionPane.showMessageDialog(null, "Futebolista Deletado");
		return true;
	}
		}
			}
		}
	

	
	public Futebolista pesquisarF(String umNome){
		for(Futebolista umFutebolista : listaFut){
			if(umFutebolista.getNome().equalsIgnoreCase(umNome))
			return umFutebolista;
		}
		return null;
	}
	
	public Tenista pesquisarT(String umNome){
		for(Tenista umTenista : listaTenis){
			if(umTenista.getNome().equalsIgnoreCase(umNome))
				return umTenista;
		}
		return null;
	}
	
	
	public String exibirF(){
		String saida= "Futebolistas";
		if(listaFut.size()>0){
	//		System.out.println("Futebolistas Cadastrados:");
		for(Futebolista umFutebolista : listaFut){
			System.out.println(umFutebolista.getNome());
			saida = (saida + "\n" + umFutebolista.getNome());
		} 
		}
		return saida; 
	}
	
	public String exibirT(){
		String saida= "Tenistas"; 
		if(listaTenis.size()>0){
			//System.out.println("Tenistas Cadastrados:");
		for(Tenista umTenista : listaTenis){
			System.out.println(umTenista.getNome());
			saida = (saida + "\n" + umTenista.getNome());
		} 
	
		} 
		return saida;
	}
	
	public String caractF(Futebolista umFutebolista){
/*	System.out.println("  Futebolista");	
	System.out.println("Nome: " + umFutebolista.getNome());
	System.out.println("Idade: " + umFutebolista.getIdade());
	System.out.println("Peso: " + umFutebolista.getPeso());*/
	Endereco oEndereco =  umFutebolista.getEndereco();
	/*System.out.println("Estado: " + oEndereco.getEstado());
	System.out.println("Estado: " + oEndereco.getCidade());
	System.out.println("Logradouro: " + oEndereco.getLogradouro());
	System.out.println("Numero: " + oEndereco.getNumero());*/
		String fut= ("  Futebolista"+"\n"+ "Nome: " + umFutebolista.getNome() + "\n" + "Idade: " + umFutebolista.getIdade()+ "\n" +"Peso: " + umFutebolista.getPeso() + "\n"+ "Estado: " + oEndereco.getEstado() + "\n"+ "Estado: " + oEndereco.getCidade()+ "\n"+ "CEP: " + oEndereco.getCep()+ "\n"+ "Posicao: " + umFutebolista.getPosicao()+ "\n");
		return fut;
	}
	
	public String caractT(Tenista umTenista){
/*	System.out.println("  Tenista");	
	System.out.println("Nome: " + umTenista.getNome());
	System.out.println("Idade: " + umTenista.getIdade());
	System.out.println("Peso: " + umTenista.getPeso());*/
	Endereco oEndereco =  umTenista.getEndereco();
/*	System.out.println("Estado: " + oEndereco.getEstado());
	System.out.println("Estado: " + oEndereco.getCidade());
	System.out.println("Logradouro: " + oEndereco.getLogradouro());
	System.out.println("Numero: " + oEndereco.getNumero());*/
	String tenis= ("  Tenista"+"\n"+ "Nome: " + umTenista.getNome() + "\n" + "Idade: " + umTenista.getIdade()+ "\n" +"Peso: " + umTenista.getPeso() + "\n"+ "Estado: " + oEndereco.getEstado() + "\n"+ "Estado: " + oEndereco.getCidade()+ "\n"+ "CEP: " + oEndereco.getCep()+ "\n"+ "Posicao: " + umTenista.getForcaSaque()+ "\n");
	return tenis;
	}
	
	public String caract(String umNome){
		Tenista umTenista= null;
		Futebolista umFutebolista=null;
		String saida="Default";
	
umFutebolista = pesquisarF(umNome);
umTenista = pesquisarT(umNome);

if ((umFutebolista == null) && (umTenista == null)) {
	//	System.out.println("Nao encontrado");
	saida ="Nao encontrado";
		} else{

if (umTenista != null)
{
//	System.out.println("Tenista");
	saida = caractT(umTenista);
}
if (umFutebolista != null)
{
//	System.out.println("Futebolista");
	saida = caractF(umFutebolista);
}
if ((umFutebolista != null)&&(umTenista != null))
{
//	System.out.println("Futebolista");
	saida = (caractF(umFutebolista) + "\n\n" + caractT(umTenista));
}
		}
return saida;
	}
	
	
}